## IVA - intentionally Vulnerable Application

- This is a Java application that are intentionaly vulnerable with purpose to check out multiple tools in the context of security, using tools in the devsecops stack such as SonarCloud, Snyk, OWASP Zap.

## SSDLC

### SAST

- Work in Progress

### SCA

- Work in Progress

### DAST

- Work in Progress

## Tools used in this project

### SonarCloud

- Work in Progress

### Snyk

- Snyk is an organization that develops security tools using SaaS approach to secure:

    * Source Code (SAST)
    * Open Source/Third Party libraries (SCA)
    * Containers
    * Infra as Code

- In this example, we will use snyk to implement SCA feature.

### OWASP Zaproxy

- Work in Progress
